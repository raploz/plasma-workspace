# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Kristóf Kiszel <ulysses@kubuntu.org>, 2014, 2015, 2019.
# Kiszel Kristóf <kiszel.kristof@gmail.com>, 2017, 2018, 2020, 2021.
# Kristof Kiszel <ulysses@fsf.hu>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-17 02:11+0000\n"
"PO-Revision-Date: 2021-12-29 17:50+0100\n"
"Last-Translator: Kristof Kiszel <ulysses@fsf.hu>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../sddm-theme/KeyboardButton.qml:20
#, kde-format
msgid "Keyboard Layout: %1"
msgstr "Billentyűzetkiosztás: %1"

#: ../sddm-theme/Login.qml:86
#, kde-format
msgid "Username"
msgstr "Felhasználónév"

#: ../sddm-theme/Login.qml:103 contents/lockscreen/MainBlock.qml:62
#, kde-format
msgid "Password"
msgstr "Jelszó"

#: ../sddm-theme/Login.qml:145 ../sddm-theme/Login.qml:151
#, kde-format
msgid "Log In"
msgstr "Bejelentkezés"

#: ../sddm-theme/Main.qml:203 contents/lockscreen/LockScreenUi.qml:305
#, kde-format
msgid "Caps Lock is on"
msgstr "A Caps Lock be van kapcsolva"

#: ../sddm-theme/Main.qml:215 ../sddm-theme/Main.qml:464
#: contents/logout/Logout.qml:165
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Alvó állapot"

#: ../sddm-theme/Main.qml:222 ../sddm-theme/Main.qml:471
#: contents/logout/Logout.qml:183
#, kde-format
msgid "Restart"
msgstr "Újraindítás"

#: ../sddm-theme/Main.qml:229 ../sddm-theme/Main.qml:478
#: contents/logout/Logout.qml:193
#, kde-format
msgid "Shut Down"
msgstr "Leállítás"

#: ../sddm-theme/Main.qml:236
#, kde-format
msgctxt "For switching to a username and password prompt"
msgid "Other…"
msgstr "Egyéb…"

#: ../sddm-theme/Main.qml:451
#, kde-format
msgid "Type in Username and Password"
msgstr "Adja meg a felhasználónevet és a jelszót"

#: ../sddm-theme/Main.qml:485
#, kde-format
msgid "List Users"
msgstr "Felhasználólista"

#: ../sddm-theme/Main.qml:552 contents/lockscreen/LockScreenUi.qml:580
#, kde-format
msgctxt "Button to show/hide virtual keyboard"
msgid "Virtual Keyboard"
msgstr "Virtuális billentyűzet"

#: ../sddm-theme/Main.qml:598
#, kde-format
msgid "Login Failed"
msgstr "Sikertelen bejelentkezés"

#: ../sddm-theme/SessionButton.qml:19
#, kde-format
msgid "Desktop Session: %1"
msgstr "Asztali munkamenet: %1"

#: contents/components/Battery.qml:55
#, kde-format
msgid "%1%"
msgstr "%1%"

#: contents/components/Battery.qml:56
#, kde-format
msgid "Battery at %1%"
msgstr "Akkumulátor: %1%"

#: contents/components/UserList.qml:72
#, kde-format
msgctxt "Nobody logged in on that session"
msgid "Unused"
msgstr "Nem használt"

#: contents/components/UserList.qml:78
#, kde-format
msgctxt "User logged in on console number"
msgid "TTY %1"
msgstr "TTY %1"

#: contents/components/UserList.qml:80
#, kde-format
msgctxt "User logged in on console (X display number)"
msgid "on TTY %1 (Display %2)"
msgstr "ezen: TTY%1 (Kijelző: %2)"

#: contents/components/UserList.qml:84
#, kde-format
msgctxt "Username (location)"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: contents/lockscreen/config.qml:19
#, fuzzy, kde-format
#| msgid "Clock"
msgctxt "@title: group"
msgid "Clock:"
msgstr "Óra"

#: contents/lockscreen/config.qml:22
#, kde-format
msgctxt "@option:check"
msgid "Keep visible when unlocking prompt disappears"
msgstr ""

#: contents/lockscreen/config.qml:33
#, fuzzy, kde-format
#| msgid "Media Controls"
msgctxt "@title: group"
msgid "Media controls:"
msgstr "Médiavezérlők"

#: contents/lockscreen/config.qml:36
#, kde-format
msgctxt "@option:check"
msgid "Show under unlocking prompt"
msgstr ""

#: contents/lockscreen/LockScreenUi.qml:40
#, kde-format
msgid "Unlocking failed"
msgstr "Sikertelen feloldás"

#: contents/lockscreen/LockScreenUi.qml:319
#, kde-format
msgid "Sleep"
msgstr "Alvó állapot"

#: contents/lockscreen/LockScreenUi.qml:325 contents/logout/Logout.qml:174
#, kde-format
msgid "Hibernate"
msgstr "Hibernálás"

#: contents/lockscreen/LockScreenUi.qml:331
#, kde-format
msgid "Switch User"
msgstr "Felhasználóváltás"

#: contents/lockscreen/LockScreenUi.qml:527
#, kde-format
msgid "Switch to This Session"
msgstr "Váltás erre a munkamenetre"

#: contents/lockscreen/LockScreenUi.qml:535
#, kde-format
msgid "Start New Session"
msgstr "Új munkamenet indítása"

#: contents/lockscreen/LockScreenUi.qml:548
#, kde-format
msgid "Back"
msgstr "Vissza"

#: contents/lockscreen/LockScreenUi.qml:594
#, kde-format
msgctxt "Button to change keyboard layout"
msgid "Switch layout"
msgstr "Kiosztásváltás"

#: contents/lockscreen/MainBlock.qml:102
#: contents/lockscreen/NoPasswordUnlock.qml:17
#, kde-format
msgid "Unlock"
msgstr "Feloldás"

#: contents/lockscreen/MediaControls.qml:112
#, kde-format
msgid "No title"
msgstr ""

#: contents/lockscreen/MediaControls.qml:113
#, kde-format
msgid "No media playing"
msgstr "Nincs médialejátszás"

#: contents/lockscreen/MediaControls.qml:142
#, kde-format
msgid "Previous track"
msgstr "Előző szám"

#: contents/lockscreen/MediaControls.qml:154
#, kde-format
msgid "Play or Pause media"
msgstr "Média indítása/szüneteltetése"

#: contents/lockscreen/MediaControls.qml:168
#, kde-format
msgid "Next track"
msgstr "Következő szám"

#: contents/logout/Logout.qml:141
#, kde-format
msgid ""
"One other user is currently logged in. If the computer is shut down or "
"restarted, that user may lose work."
msgid_plural ""
"%1 other users are currently logged in. If the computer is shut down or "
"restarted, those users may lose work."
msgstr[0] ""
"Másik felhasználó is bejelentkezve van. Ha a számítógépet leállítja vagy "
"újraindítja, az ő munkája elveszhet."
msgstr[1] ""
"Más felhasználók (%1) is bejelentkezve vannak. Ha a számítógépet leállítja "
"vagy újraindítja, az ő munkájuk elveszhet."

#: contents/logout/Logout.qml:155
#, kde-format
msgid "When restarted, the computer will enter the firmware setup screen."
msgstr "Újraindításkor a számítógép a firmware-telepítő képernyőre fog lépni."

#: contents/logout/Logout.qml:203
#, kde-format
msgid "Log Out"
msgstr "Kijelentkezés"

#: contents/logout/Logout.qml:226
#, kde-format
msgid "Restarting in 1 second"
msgid_plural "Restarting in %1 seconds"
msgstr[0] "Újraindítás 1 másodperc múlva"
msgstr[1] "Újraindítás %1 másodperc múlva"

#: contents/logout/Logout.qml:228
#, kde-format
msgid "Shutting down in 1 second"
msgid_plural "Shutting down in %1 seconds"
msgstr[0] "Újraindítás 1 másodperc múlva"
msgstr[1] "Leállítás %1 másodperc múlva"

#: contents/logout/Logout.qml:230
#, kde-format
msgid "Logging out in 1 second"
msgid_plural "Logging out in %1 seconds"
msgstr[0] "Kijelentkezés 1 másodperc múlva"
msgstr[1] "Kijelentkezés %1 másodperc múlva"

#: contents/logout/Logout.qml:241
#, kde-format
msgid "OK"
msgstr "OK"

#: contents/logout/Logout.qml:247
#, kde-format
msgid "Cancel"
msgstr "Mégsem"

#: contents/osd/OsdItem.qml:69
#, kde-format
msgid "100%"
msgstr "100%"

#: contents/osd/OsdItem.qml:83
#, kde-format
msgctxt "Percentage value"
msgid "%1%"
msgstr "%1%"

#: contents/splash/Splash.qml:81
#, kde-format
msgctxt ""
"This is the first text the user sees while starting in the splash screen, "
"should be translated as something short, is a form that can be seen on a "
"product. Plasma is the project name so shouldn't be translated."
msgid "Plasma made by KDE"
msgstr "Plasma, a KDE-től"

#~ msgid "Configure"
#~ msgstr "Beállítás"

#, fuzzy
#~| msgid "Configure KRunner…"
#~ msgid "Configure KRunner Behavior"
#~ msgstr "A KRunner beállításai…"

#~ msgid "Configure KRunner…"
#~ msgstr "A KRunner beállításai…"

#~ msgctxt "Textfield placeholder text, query specific KRunner"
#~ msgid "Search '%1'…"
#~ msgstr "Keresés: „%1”…"

#~ msgctxt "Textfield placeholder text"
#~ msgid "Search…"
#~ msgstr "Keresés…"

#~ msgid "Show Usage Help"
#~ msgstr "Használati súgó megjelenítése"

#~ msgid "Pin"
#~ msgstr "Rögzítés"

#~ msgid "Pin Search"
#~ msgstr "Keresés rögzítése"

#~ msgid "Keep Open"
#~ msgstr "Tartsa nyitva"

#~ msgid "Recent Queries"
#~ msgstr "Legutóbbi lekérdezések"

#~ msgid "Remove"
#~ msgstr "Eltávolítás"

#~ msgid "in category recent queries"
#~ msgstr "a legutóbbi lekérdezések kategóriában"

#~ msgctxt "verb, to show something"
#~ msgid "Show:"
#~ msgstr "Megjelenítés:"

#~ msgid "Configure Search Plugins"
#~ msgstr "Kereső bővítmények beállítása"

#~ msgctxt ""
#~ "This is the first text the user sees while starting in the splash screen, "
#~ "should be translated as something short, is a form that can be seen on a "
#~ "product. Plasma is the project name so shouldn't be translated."
#~ msgid "Plasma 25th Anniversary Edition by KDE"
#~ msgstr "A Plasma 25. évfordulós kiadása a KDE-től"

#~ msgid "Close"
#~ msgstr "Bezárás"

#~ msgctxt "verb, to show something"
#~ msgid "Always show"
#~ msgstr "Mindig látszódjon"

#~ msgid "Suspend"
#~ msgstr "Felfüggesztés"

#~ msgid "Reboot"
#~ msgstr "Újraindítás"

#~ msgid "Logout"
#~ msgstr "Kijelentkezés"

#~ msgid "Different User"
#~ msgstr "Másik felhasználó"

#, fuzzy
#~| msgid "Login as different user"
#~ msgid "Log in as a different user"
#~ msgstr "Bejelentkezés másik felhasználóval"

#, fuzzy
#~| msgid "Password"
#~ msgid "Password..."
#~ msgstr "Jelszó"

#~ msgid "Login"
#~ msgstr "Bejelentkezés"

#~ msgid "Shutdown"
#~ msgstr "Kikapcsolás"

#~ msgid "Switch"
#~ msgstr "Váltás"

#~ msgid "New Session"
#~ msgstr "Új munkamenet"

#~ msgid "%1%. Charging"
#~ msgstr "%1%. Töltés"

#~ msgid "Fully charged"
#~ msgstr "Teljesen feltöltött"

#~ msgid "%1% battery remaining"
#~ msgstr "%1% van hátra"

#~ msgid "Change Session"
#~ msgstr "Munkamenetváltás"

#~ msgid "Create Session"
#~ msgstr "Munkamenet létrehozása"

#~ msgid "Change Session..."
#~ msgstr "Munkamenetváltás…"

#, fuzzy
#~| msgid "%1 (%2)"
#~ msgctxt "Username (logged in on console)"
#~ msgid "%1 (TTY)"
#~ msgstr "%1 (%2)"

#~ msgctxt "Button to restart the computer"
#~ msgid "Reboot"
#~ msgstr "Újraindítás"

#, fuzzy
#~| msgid "Shut down"
#~ msgctxt "Button to shut down the computer"
#~ msgid "Shut down"
#~ msgstr "Leállítás"

#, fuzzy
#~| msgid "Logging out"
#~ msgctxt "Dialog heading, confirm log out, not a status label"
#~ msgid "Logging out"
#~ msgstr "Kijelentkezés"

#, fuzzy
#~| msgid "Rebooting"
#~ msgctxt "Dialog heading, confirm reboot, not a status label"
#~ msgid "Rebooting"
#~ msgstr "Újraindítás"

#, fuzzy
#~| msgid "%1 (%2)"
#~ msgctxt "Username (on display number)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

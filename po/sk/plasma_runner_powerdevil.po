# translation of plasma_runner_powerdevil.po to Slovak
# Peter Mihalik <udavac@inmail.sk>, 2009.
# Michal Sulek <misurel@gmail.com>, 2009, 2010.
# Mthw <jari_45@hotmail.com>, 2018, 2019.
# Matej Mrenica <matejm98mthw@gmail.com>, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma_runner_powerdevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-10 02:12+0000\n"
"PO-Revision-Date: 2023-01-11 21:30+0100\n"
"Last-Translator: Matej Mrenica <matejm98mthw@gmail.com>\n"
"Language-Team: Slovak <kde-i18n-doc@kde.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.1\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: PowerDevilRunner.cpp:31
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "suspend"
msgstr "uspat"

#: PowerDevilRunner.cpp:33
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "to ram"
msgstr "do ram"

#: PowerDevilRunner.cpp:35
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "sleep"
msgstr "uspat"

#: PowerDevilRunner.cpp:37
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "hibernate"
msgstr "hibernovat"

#: PowerDevilRunner.cpp:39
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "to disk"
msgstr "na disk"

#: PowerDevilRunner.cpp:41 PowerDevilRunner.cpp:43 PowerDevilRunner.cpp:64
#, kde-format
msgctxt "Note this is a KRunner keyword"
msgid "dim screen"
msgstr "stmaviet obrazovku"

#: PowerDevilRunner.cpp:52
#, kde-format
msgid ""
"Lists system suspend (e.g. sleep, hibernate) options and allows them to be "
"activated"
msgstr ""
"Zobrazí možnosti uspania (napr. do RAM, na disk) a umožní ich aktivovať"

#: PowerDevilRunner.cpp:56
#, kde-format
msgid "Suspends the system to RAM"
msgstr "Uspí systém do RAM"

#: PowerDevilRunner.cpp:60
#, kde-format
msgid "Suspends the system to disk"
msgstr "Uspí systém na disk"

#: PowerDevilRunner.cpp:63
#, kde-format
msgctxt ""
"Note this is a KRunner keyword, <> is a placeholder and should be at the end"
msgid "screen brightness <percent value>"
msgstr "jas obrazovky <percent value>"

#: PowerDevilRunner.cpp:66
#, no-c-format, kde-format
msgid ""
"Lists screen brightness options or sets it to the brightness defined by the "
"search term; e.g. screen brightness 50 would dim the screen to 50% maximum "
"brightness"
msgstr ""
"Uvádza zoznam možností jasu obrazovky alebo ho nastavuje na jas definovaný "
"hľadaným výrazom; napr. jas obrazovky 50 stlmí obrazovku na maximálny jas 50 "
"%."

#: PowerDevilRunner.cpp:88
#, kde-format
msgid "Set Brightness to %1%"
msgstr "Nastaviť jas na %1%"

#: PowerDevilRunner.cpp:97
#, kde-format
msgid "Dim screen totally"
msgstr "Stmavieť obrazovku úplne"

#: PowerDevilRunner.cpp:105
#, kde-format
msgid "Dim screen by half"
msgstr "Stmavieť obrazovku na polovicu"

#: PowerDevilRunner.cpp:135
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Uspať"

#: PowerDevilRunner.cpp:136
#, kde-format
msgid "Suspend to RAM"
msgstr "Uspať do RAM"

#: PowerDevilRunner.cpp:141
#, kde-format
msgctxt "Suspend to disk"
msgid "Hibernate"
msgstr "Hibernovať"

#: PowerDevilRunner.cpp:142
#, kde-format
msgid "Suspend to disk"
msgstr "Uspať na disk"

#: PowerDevilRunner.cpp:210
#, kde-format
msgctxt "Note this is a KRunner keyword, it should end with a space"
msgid "screen brightness "
msgstr "jas obrazovky "

#: PowerDevilRunner.cpp:212
#, kde-format
msgctxt "Note this is a KRunner keyword, it should end with a space"
msgid "dim screen "
msgstr "stmaviť obrazovku"

#~ msgctxt "Note this is a KRunner keyword"
#~ msgid "screen brightness"
#~ msgstr "jas obrazovky"

#~ msgctxt "Note this is a KRunner keyword; %1 is a parameter"
#~ msgid "screen brightness %1"
#~ msgstr "jas obrazovky %1"

#~ msgctxt "Note this is a KRunner keyword; %1 is a parameter"
#~ msgid "dim screen %1"
#~ msgstr "stmaviet obrazovku %1"

#~ msgid "Suspend"
#~ msgstr "Uspať"

#~ msgctxt "Note this is a KRunner keyword"
#~ msgid "power profile"
#~ msgstr "profil napajania"

#~ msgid "Lists all power profiles and allows them to be activated"
#~ msgstr "Zobrazí všetky profily napájania a umožní ich aktivovať"

#~ msgctxt "Note this is a KRunner keyword; %1 is a parameter"
#~ msgid "power profile %1"
#~ msgstr "profil napajania %1"

#~ msgid "Set Profile to '%1'"
#~ msgstr "Nastaviť profil na '%1'"

#~ msgid "Turn off screen"
#~ msgstr "Vypnúť obrazovku"
